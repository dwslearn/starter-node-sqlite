/* 
Imports
*/
const sqlite3 = require('sqlite3').verbose(); //=> https://www.npmjs.com/package/sqlite3
//

/* 
Methods
*/
    const createTables = (sqliteDb) => {
        sqliteDb.run(`
            CREATE TABLE IF NOT EXISTS capteurs (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name TEXT NOT NULL,
                latitude INTEGER NOT NULL,
                longitude INTEGER NOT NULL
            )
        `, 
        function (err){
            console.log('createTables error:', err, sqliteDb)
            return err ? err : sqliteDb;
        });
    }

    const createSqliteItem = (sqliteDb, table, column, value) => {
        return new Promise( (resolve, reject) => {
            sqliteDb.serialize( () => {
                sqliteDb.run(
                    `INSERT INTO ${table} (${column}) VALUES(${value})`, 
                    function (err){
                        if( err ){ return resolve(err) }
                        else{
                            sqliteDb.each(`SELECT * FROM ${table}`, (err, row) => {
                                if( err ){ return resolve(err) }
                                else{
                                    if( row.id === this.lastID ){ return resolve(row) }
                                }
                            });
                        }
                    }
                );
            })
        })
    }

    const getTableContent = (sqliteDb, table) =>{
        return new Promise( (resolve, reject) => {
            sqliteDb.all(`SELECT * FROM ${table}`, (err, row) => {
                if( err ){ return resolve(err) }
                else{ return resolve(row) }
            });
        })
    }
//

/* 
Export
*/
    module.exports = {
        createSqliteItem,
        createTables,
        getTableContent
    }
//