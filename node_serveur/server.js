/* 
Imports
*/
    // NPM modules
    require('dotenv').config(); //=> https://www.npmjs.com/package/dotenv
    const express = require('express'); //=> https://www.npmjs.com/package/express
    const path = require('path'); //=> https://www.npmjs.com/package/path
    const sqlite3 = require('sqlite3').verbose(); //=> https://www.npmjs.com/package/sqlite3

    // Inner
    const { createSqliteItem, createTables, getTableContent } = require('./services/sqlite.service');
//


/*
Server class
*/
    class ServerClass{
        constructor(){
            this.server = express();
            this.port = process.env.PORT;
            this.sqliteDb = undefined;
        }

        init(){
            //=> Set server view engine: use EJS (https://ejs.co)
            this.server.set( 'view engine', 'ejs' );

            //=> Static path configuration: define 'www' folder for backoffice static files
            this.server.set( 'views', __dirname + '/www' );
            this.server.use( express.static(path.join(__dirname, 'www')) );

            //=> Set body request with ExpressJS: BodyParser not needed (http://expressjs.com/fr/api.html#express.json)
            this.server.use(express.json({limit: '20mb'}));
            this.server.use(express.urlencoded({ extended: true }))

            // Set up SQLite
            this.sqliteDb = new sqlite3.Database('../data/sqlite.db');

            // Start server configuration
            this.config(this.sqliteDb);
        }

        async config(sqliteDb){
            console.log(sqliteDb)

            // Create Sqlite Table
            createTables(sqliteDb);

            // Create back office route for home page
            this.server.get('/', (req, res) => {
                // Get data from Sqlite
                getTableContent(sqliteDb, 'capteurs')
                .then( sqliteData => {
                    console.log(sqliteData)
                    return res.render('index', { data: sqliteData });
                })
            })

            // Create back office route for add data
            this.server.get('/add-data', (req, res) => {
                return res.render('form');
            })

            // Create back office route for add data
            this.server.post('/post-data', (req, res) => {
                // Add data in Sqlite
                createSqliteItem(sqliteDb, 'capteurs', 'name, latitude, longitude', `"${req.body.name}", ${req.body.latitude}, ${req.body.longitude}`)
                .then( newItem => {
                    return res.redirect('/');
                })
            })

            // Launch server
            this.launch();
        }

        launch(){
            this.server.listen(this.port, () => {
                console.log({
                    node: `http://localhost:${this.port}`,
                });
            });
        }
    }
//

/* 
Start server
*/
    const NodeApp = new ServerClass();
    NodeApp.init();
//